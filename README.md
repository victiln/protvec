# Klasy i narzędzia stworzone podczas pisania licencjatu (eng below)

Celem licencjatu była klasyfikacja białek na podstawie ich sekwencji pierwszorzędowej z użyciem metod reprezentacji jako etapów pośrednich.

Centralną metodą jest ProtVec (Ehsaneddin Asgari, Mohammad R.K. Mofrad, 2015). 

Moja implementacja bazuje na [implementacji](https://github.com/kyu999/biovec) kyu99.

Do klasyfikacji użyte zostały:
- Lasy losowe
- Maszyny wektorów nośnych
- Sztuczne sieci neuronowe



# My ProtVec and other stuff

In this repo I store functions and classes that I use with my bachelor's thesis.
It involves classifying proteins to classes based on their primary sequence.
<p>To represent protein sequences I use ProtVec, described in Ehsaneddin Asgari, Mohammad R.K. Mofrad, 2015. 
My implementation is based on [implementation](https://github.com/kyu999/biovec) by kyu99. 
It uses Word2Vec from gensim.</p>

To classify embedded protein sequences I use:
* Support Vector Machines and Random Forests implemented in scikit-learn
* Artificial neural networks using pytorch package


# License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details