
Importy bibliotek


```python
import numpy as np
from Bio import SeqIO

from source.models import ProtVec
from source.data_load import DataHolder
from source.util import time_context_manager

```

Źródła danych


```python
rhodo_fname = "data/sample_rhodo.fasta"
trans_fname = "data/sample_trans.fasta"
ntrans_fname = "data/sample_ntrans.fasta"

all_proteins_fname = "data/all_sample_proteins.fasta"
```

### Reprezentacja

Trening ProtVeca z domyślnymi parametrami


```python
with time_context_manager("ProtVec training"):
    pv_model = ProtVec(all_proteins_fname)
```

    ProtVec training took: 3.175748 s


Zapis modelu


```python
pv_model.save("models/ProtVec_model.model")
```

Wczytywanie danych i reprezentacji według wyżej wytrenowanego modelu\

Rodopsynom jest przypisana klasa 1, transbłonowym innym niż rodopsyny zero i nietransbłonowym również klasa 0


```python
model_fname = "models/ProtVec_model.model"
fasta_filenames = [rhodo_fname, trans_fname, ntrans_fname]
datasets_classes = [("rhodo", 1), ("trans", 0), ("ntrans", 0)]

data = DataHolder.load_with_protvec(model_fname, fasta_filenames, datasets_classes)
```

Rzut oka na dane


```python
print(data)
print()
print(data[0])
print()
print(data[-1])
print()
print(data.rhodo) # dostęp do źródeł danych za pomocą atrybutów
```

    DataHolder with 1500 sequences
    
    <Sequence object at 0x1a19842518
    Record name: A0A0B4V767
    Class: 1, Dataset: rhodo>
    
    <Sequence object at 0x1077376d8
    Record name: sp|Q6GJI1|SYM_STAAR Methionine--tRNA ligase OS=Staphylococcus aureus (strain MRSA252) OX=282458 GN=metG PE=3 SV=1
    Class: 0, Dataset: ntrans>
    
    DataHolder with 500 sequences


Importowanie bibliotek do uczenia


```python
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
import skorch
from torch import nn
from torch import optim
import torch.nn.functional as F
import torch
```

Przygotowanie danych do modeli


```python
X = np.array([x.protvec_repr for x in data]).reshape(-1, 100).astype('float32')
y = np.array([x.cls for x in data]).reshape(-1, 1).astype('float32')
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, stratify=y)
```

### Klasyfikacja

Las losowy


```python
rf = RandomForestClassifier(criterion="gini", min_samples_split=2, n_estimators=64)
with time_context_manager("RF training"):
    rf.fit(X_train, y_train.reshape(-1))
print(classification_report(y_test, 
                            rf.predict(X_test), 
                           )
     )
```

    RF training took: 0.276916 s
                  precision    recall  f1-score   support
    
             0.0       0.90      0.94      0.92       200
             1.0       0.87      0.78      0.82       100
    
       micro avg       0.89      0.89      0.89       300
       macro avg       0.88      0.86      0.87       300
    weighted avg       0.89      0.89      0.89       300
    


SVM


```python
svm = SVC(kernel='rbf', C=1000, gamma='scale', class_weight='balanced', verbose=True)
with time_context_manager("SVM training"):
    svm.fit(X_train, y_train.reshape(-1))
print(classification_report(y_test, 
                            svm.predict(X_test), 
                           )
     )
```

    [LibSVM]SVM training took: 0.150447 s
                  precision    recall  f1-score   support
    
             0.0       0.93      0.94      0.94       200
             1.0       0.89      0.86      0.87       100
    
       micro avg       0.92      0.92      0.92       300
       macro avg       0.91      0.90      0.91       300
    weighted avg       0.92      0.92      0.92       300
    


Sieć neuronowa


```python
dim_in=100
hidden_1=1024
hidden_2=1024
hidden_3=1024
class MyNNet(nn.Module):
    
    def __init__(self, dim_in, hidden_1, hidden_2, hidden_3):
        super(MyNNet, self).__init__()
        self.model = nn.Sequential(nn.Linear(dim_in, hidden_1),
                                   nn.ReLU(),
                                   nn.Linear(hidden_1, hidden_2),
                                   nn.ReLU(),
                                   nn.Linear(hidden_2, hidden_3),
                                   nn.ReLU(),
                                   nn.Linear(hidden_3, 1),
                                   nn.Sigmoid(),)
        
    def forward(self, x):
        return self.model(x)
```


```python
nnet = skorch.NeuralNetClassifier(MyNNet,
                                  optimizer=optim.Adam,
                                  lr=1e-4,
                                  criterion = nn.BCELoss,
                                  module__dim_in=dim_in,
                                  module__hidden_1=hidden_1,
                                  module__hidden_2=hidden_2,
                                  module__hidden_3=hidden_3,
                                  train_split=False,
                                  max_epochs=10,
                                  batch_size=8, 
                                 )
with time_context_manager("NNet training"):
    nnet.fit(X_train, y_train)
print(classification_report(y_test, 
                            [1 if x > 0.5 else 0 for x in nnet.predict_proba(X_test)], 
                           )
     )
```

      epoch    train_loss     dur
    -------  ------------  ------
          1        [36m0.6138[0m  2.3891
          2        [36m0.4579[0m  2.3735
          3        [36m0.4082[0m  2.5090
          4        [36m0.3812[0m  2.3588
          5        [36m0.3665[0m  3.4901
          6        0.3667  5.1597
          7        [36m0.3607[0m  5.2070
          8        [36m0.3599[0m  5.4814
          9        [36m0.3582[0m  5.5858
         10        0.3587  5.5951
    NNet training took: 40.219963 s
                  precision    recall  f1-score   support
    
             0.0       0.82      0.99      0.90       200
             1.0       0.97      0.58      0.72       100
    
       micro avg       0.85      0.85      0.85       300
       macro avg       0.90      0.78      0.81       300
    weighted avg       0.87      0.85      0.84       300
    


Komentarz: sieć neuronowa potrzebuje dużo więcej danych niż inne metody, żeby dać sensowne wyniki.


```python

```
